/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculadora_examen;

import java.util.Scanner;

/**
 *
 * @author DAM124
 */
public class calculadoraexamen {
    static Scanner scanner=new Scanner(System.in);
    static int opcion=-1;//opción de menú
    static float numero1=0, numero2=0;//variables de entrada
    public static void main(String[] args) {
        while(opcion!=0){
            //try catch para evitar que el programa termine si hay un error
            try{
                System.out.println("Elige opción:\n"
                        + "1.Sumar\n"
                        + "2.Restar\n"
                        + "3.Multiplicar\n"
                        + "4.Dividir\n"
                        + "5.Porcentaje\n"
                        + "0.Salir");
                System.out.println("Selecciona una opción del 0 al 4");
                opcion=Integer.parseInt(scanner.nextLine());
                switch(opcion){
                    case 1: 
                        pideNumeros();
                        System.out.println(numero1+"+"+numero2+"="+(numero1+numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1+"+"+numero2+"="+(numero1-numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1+"+"+numero2+"="+(numero1*numero2));
                        break;
                    case 4:
                        pideNumeros();
                        System.out.println(numero1+"+"+numero2+"="+(numero1/numero2));
                        break;
                    case 5:
                        pideNumeros();
                        System.out.println("El porcentaje de "+numero1+" de un "+numero2+"% es de "+(numero1*numero2)/100);
                    case 0:
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no disponible");
                        break;
                }
                System.out.println("\n");
            }catch(Exception e){
                System.out.println("¡Error!");
            }
        }
    }
    //método para recoger las variables de entrada
    public static void pideNumeros(){
        System.out.println("Introduce el primer número:");
        numero1=Integer.parseInt(scanner.nextLine());
        System.out.println("Introduce el segundo número:");
        numero2=Integer.parseInt(scanner.nextLine());
    }
            
}
